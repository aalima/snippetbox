module se02.com

go 1.15

require (
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.10.1
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/text v0.3.4 // indirect
)
