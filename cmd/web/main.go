package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime/debug"
	"se02.com/pkg/models/postgresql"
)

type application struct {
	infoLog  *log.Logger
	errorLog *log.Logger
	snippets *postgresql.SnippetModel
}

func (app *application) serverError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	app.errorLog.Println(trace)
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func (app *application) clientError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

func (app *application) notFound(w http.ResponseWriter) {
	app.clientError(w, http.StatusNotFound)
}

func (app *application) routes() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", app.home)
	mux.HandleFunc("/snippet", app.showSnippet)
	mux.HandleFunc("/snippet/create", app.createSnippet)
	fileServer := http.FileServer(http.Dir("./ui/static/"))
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))
	return mux
}


func main() {
	addr :=flag.String("addr", ":4000", "HTTP network Access")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate | log.Ltime)
	errLog := log.New(os.Stderr, "ERROR\t", log.Ldate | log.Ltime | log.Lshortfile)

	config := pgx.ConnConfig{
		Host: "localhost",
		Port: 5433,
		Database: "postgres",
		User: "postgres",
		Password: "postgres",
	}
	conn, err := pgx.Connect(config)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer conn.Close()

	app := application{
		infoLog:  infoLog,
		errorLog: errLog,
		snippets: dbconn,
	}

	server := http.Server{
		Addr: *addr,
		Handler: app.routes(),
		ErrorLog: errLog,
	}

	infoLog.Printf("Server started on port: %v", addr)
	err1 := server.ListenAndServe()
	if err1 != nil {
		errLog.Fatal(err)
	}
}
